import client
import menu
from client import Client


class Llibreta:
    id_client = 0
    llista_client = []

    def __init__(self):
        self.llista_client = []
        self.id_client = 0

    def get_llista_clients(self):
        for client in self.llista_client:
            print(client)

    def afegir_client(self):
        nom = input("Introdueix el nom del client: ")
        cognom = input("Introdueix el cognom del client: ")
        telefon = input("Introdueix el telèfon del client: ")
        correu = input("Introdueix el correu del client: ")
        adreça = input("Introdueix l'adreça del client: ")
        ciutat = input("Introdueix la ciutat del client: ")

        client = Client(nom, cognom, telefon, correu, adreça, ciutat)
        self.id_client += 1
        client.id_client = self.id_client

        print("\nClient afegit correctament\n")
        self.llista_client.append(client)

    def eliminar_client(self, id):
        trobat = False
        index = 0
        longitud = len(self.llista_client)

        while not trobat and index < longitud:
            client = self.llista_client[index]

            if client.id_client == id:
                self.llista_client.remove(client)
                trobat = True
            else:
                index += 1

        if trobat:
            print("\nEl client s'ha eliminat correctament\n")
        else:
            print("\nNo s'ha trobat cap client amb aquesta ID\n")

    def cerca_per_id(self, id):
        clients_trobats = []
        for client in self.llista_client:
           if client.id_client == id:
               return  client
               clients_trobats.append(client)

        return clients_trobats

    def cerca_per_nom(self, nom_client):
        clients_trobats = []
        for client in self.llista_client:
            if client.nom == nom_client:
                clients_trobats.append(client)

        return clients_trobats

    def cerca_per_cognom(self, cognom_client):
        clients_trobats = []
        for client in self.llista_client:
            if client.cognom == cognom_client:
                clients_trobats.append(client)

        return clients_trobats

    # def modificar_camp(self, id):

    def get_llista_per_nom(self):
        llista_clients_ordenada = sorted(self.llista_client, key=lambda client: client.nom)
        for client in llista_clients_ordenada:
            print(client)
