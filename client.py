class Client:
    id_client = 0
    nom = ""
    cognom = ""
    telefon = None
    correu = ""
    adreca = ""
    ciutat = ""

    def __init__(self, nom, cognom, telefon, correu, adreca, ciutat):
        self.id_client = 0
        self.nom = nom
        self.cognom = cognom
        self.telefon = telefon
        self.correu = correu
        self.adreca = adreca
        self.ciutat = ciutat

    def __str__(self):
        return "Identificador: " + str(
            self.id_client) + "\nNom: " + self.nom + "\nCognom: " + self.cognom + "\nTelefon: " + str(
            self.telefon) + "\nCorreu: " + self.correu + "\nAdreca: " + self.adreca + "\nCiutat: " + self.ciutat + " \n"
