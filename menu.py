subtitle = "Seleccioni una opció i premi Intro"
menuchar = len(subtitle) * "="


def mostrar_menu_principal():
    print("MENU PRINCIPAL")
    print(f"{menuchar}\n{subtitle}\n{menuchar}")
    print("\t1. Afegir client \n"
          "\t2. Eliminar client \n"
          "\t3. Consultar client \n"
          "\t4. Modificar un camp d'un client (*) \n"
          "\t5. Sortir \n")


def mostrar_menu_consulta():
    print("MENU CONSULTA")
    print(f"{menuchar}\n{subtitle}\n{menuchar}")
    print("\t1. Cercar client per Identificador \n"
          "\t2. Cercar client per Nom \n"
          "\t3. Cercar client per Cognom \n"
          "\t4. Llistar tots els clients \n"
          "\t5. Llistat tots els clients per Nom (*) \n"
          "\t6. Enrere \n")

def mostrar_camps_modificar():
    print("CAMPS A MODIFICAR")
    print(f"{menuchar}\n{subtitle}\n{menuchar}")
    print("\t1. Nom \n"
          "\t2. Cognom \n"
          "\t3. Telèfon \n"
          "\t4. Correu \n"
          "\t5. Adreça \n"
          "\t6. Ciutat \n")
