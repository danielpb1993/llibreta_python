import menu
from llibreta import Llibreta
llibreta = Llibreta()

if __name__ == '__main__':

    main_menu_off = False
    menu_consulta = False
    menu_modificar = False

    while not main_menu_off:
        menu.mostrar_menu_principal()
        option = int(input("Enter an option: "))

        if option == 1:
            llibreta.afegir_client()

        elif option == 2:
            id_client_eliminar = int(input("Introdueix la ID del client que vols eliminar: "))
            llibreta.eliminar_client(id_client_eliminar)

        elif option == 3:
            main_menu_off = True
            menu_consulta = False

            while not menu_consulta:
                menu.mostrar_menu_consulta()
                option = int(input("Enter an option: "))

                if option == 1:
                    id_client_trobar = int(input("Introdueix la ID del client que vols trobar: "))
                    client_trobat = llibreta.cerca_per_id(id_client_trobar)

                    if not client_trobat:
                        print("\nNo s'ha trobat cap client amb aquesta ID\n")
                    else:
                        for client in client_trobat:
                            print(client)

                elif option == 2:
                    nom_client_trobar = input("Introdueix el nom dels clients que vols trobar: ")
                    client_trobat = llibreta.cerca_per_nom(nom_client_trobar)

                    if not client_trobat:
                        print("\nNo s'ha trobat cap client amb aquest nom\n")
                    else:
                        for client in client_trobat:
                            print(client)

                elif option == 3:
                    cognom_client_trobar = input("Introdueix el cognom dels clients que vols trobar: ")
                    client_trobat = llibreta.cerca_per_cognom(cognom_client_trobar)

                    if not client_trobat:
                        print("\nNo s'ha trobat cap client amb aquest cognom\n")
                    else:
                        for client in client_trobat:
                            print(client)

                elif option == 4:
                    llibreta.get_llista_clients()

                elif option == 5:
                    llibreta.get_llista_per_nom()

                if option == 6:
                    menu_consulta = True
                    main_menu_off = False

        elif option == 4:
            main_menu_off = True
            menu_modificar = False

            while not menu_modificar:
                id_client_modificar = int(input("Quina és la ID del client del qual vols modificar dades? "))
                # llibreta.modificar_camp(id_client_modificar)
                menu_modificar = True
                main_menu_off = False


        elif option == 5:
            main_menu_off = True

        else:
            print("Introdueix una opció correcta: ")
